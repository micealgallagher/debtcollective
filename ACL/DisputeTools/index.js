module.exports = {
  Visitor: [
    ['index', true],
    ['show', false],
  ],
  User: [
    ['index', true],
    ['show', true],
  ],
  Admin: [
    ['index', true],
    ['show', true],
  ],
};
